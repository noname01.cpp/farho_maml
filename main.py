from train import farho_train, farho_test
from model import FCModel
from data_generator import DataGenerator
import numpy as np
import tensorflow as tf
from tensorflow.python.platform import flags

FLAGS = flags.FLAGS

flags.DEFINE_bool('train', True, 'perform training if true, and testing if false')

def main():
  options = {}
  options['update_batch_size'] = 10
  options['meta_batch_size'] = 1
  options['num_updates'] = 5
  input_generator = DataGenerator(num_samples_per_class=options['update_batch_size'], batch_size=options['meta_batch_size'])
  maml_model = FCModel()
  if FLAGS.train:
    farho_train(maml_model, input_generator, options)
  else:
    test_data_generator = DataGenerator(num_samples_per_class=options['update_batch_size'], batch_size=1)
    farho_test(maml_model, test_data_generator, options)


if __name__ == '__main__':
  main()
