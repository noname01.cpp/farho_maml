from __future__ import print_function
import numpy as np
import tensorflow as tf
import abc
import far_ho as far

class MAMLModel:
  __metaclass__ = abc.ABCMeta

  def __init__(self, add_regularization=False):
    self.add_regularization = add_regularization

  @abc.abstractmethod
  def create_model_fn(self, inputs, is_training=None, activation=None, scale=0.01, prefix=''):
    """Creates the model and returns the logits and endpoints
    """
    return

  @abc.abstractmethod
  def loss_fn(self, logits, labels, is_regression=True):
    """Computes the loss function and returns loss and accuracy
    """
    return

  def create_learnable_initializers(self, variables, replace_from=None, replace_to=''):
    """Creates initializers corresponding to variables
       Args:
         variables: iterator of variables which we want to create initializers
       Returns:
         the mapping from variable in variables to their corresponding initializers
    """
    var_map = {}
    for v in variables:
      new_name = v.op.name
      if replace_from is not None:
        new_name = new_name.replace(replace_from, replace_to)
      init_var = tf.Variable(v.initialized_value(),
                             name=new_name,
                             collections=far.HYPERPARAMETERS_COLLECTIONS,
                             trainable=False)
      var_map[v] = init_var
    return var_map

  def make_variables_constant(self, variables):
    """removes variables from trainable collection
       Args:
        variables: list of variables
    """
    far.utils.remove_from_collection(far.GraphKeys.TRAINABLE_VARIABLES, *variables)

  def make_variables_hyperparameter(self, variables):
    """removes variables from trainable collection and adds them to hyperparameters collection.
       Args:
         variables: list of variables
    """
    self.make_variables_constant(variables) #remove from trianables
    for v in variables:
      tf.add_to_collection('hyperparameters', v)

class FCModel(MAMLModel):
  def __init__(self, add_regularization=False,
                     dim_input=1,
                     dim_output=1,
                     dim_hidden=[40,40],
                     norm=None): #TODO assert: dim_hidden > 0, etc, batch_norm?
    super(FCModel, self).__init__(add_regularization)
    self.dim_hidden = dim_hidden
    self.dim_input  = dim_input
    self.dim_output = dim_output
    self.norm       = norm
    self.endpoints  = {}
    self.logits     = None

  def create_model_fn(self, inputs, is_training=None, activation=tf.nn.relu, scale=0.01, prefix=''):
    input_layer = tf.reshape(inputs, [-1, self.dim_input])
    regularizer = tf.contrib.layers.l2_regularizer(scale=scale) if self.add_regularization else None
    self.endpoints[prefix+'input'] = input_layer
    with tf.variable_scope(prefix+'dense0'):
      dense = tf.layers.dense(inputs=input_layer,
                              units=self.dim_hidden[0],
                              activation=activation,
                              kernel_regularizer=regularizer)
      self.endpoints[prefix+'dense0'] = dense

    for i in range(1,len(self.dim_hidden)):
      with tf.variable_scope(prefix+'dense'+str(i)):
        dense = tf.layers.dense(inputs=dense,
                                units=self.dim_hidden[i],
                                activation=activation,
                                kernel_regularizer=regularizer)
        self.endpoints[prefix+'dense'+str(i)] = dense

    with tf.variable_scope(prefix+'logits'):
      self.logits = logits =  tf.layers.dense(inputs=dense,
                               units=self.dim_output,
                               activation=None,
                               kernel_regularizer=regularizer)
      self.endpoints[prefix+'logits'] = logits

    return [self.logits, self.endpoints]

  def loss_fn(self, logits, labels, is_regression=True):
    accuracy = None
    if is_regression:
      pred = tf.reshape(logits, [-1])
      label = tf.reshape(labels, [-1])
      output_loss = tf.reduce_mean(tf.square(pred-label))
      tf.summary.scalar('output_loss', output_loss)
      if not self.add_regularization:
        return [output_loss, accuracy]
    else:
      output_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=labels))
      accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(labels,1), tf.argmax(logits,1)), tf.float32))
      tf.summary.scalar('output_loss', output_loss)
      tf.summary.scalar('accuracy', accuracy)
      if not self.add_regularization:
        return [output_loss, accuracy]

    regu_losses = tf.losses.get_regularization_losses()
    regularization_loss = tf.add_n(regu_losses)
    tf.summary.scalar('regularization_loss', regularization_loss)
    loss = output_loss + regularization_loss
    tf.summary.scalar('total_loss', loss)
    return [loss, accuracy]

