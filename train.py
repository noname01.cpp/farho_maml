from __future__ import print_function
from model import MAMLModel
import far_ho as far
import numpy as np
import os
import tensorflow as tf


def farho_train(maml_model, input_generator, options):
  experiment         = options.get('experiment', 'sinusoid')
  add_regularization = options.get('add_regularization', False)
  is_regression      = options.get('is_regression', True)
  is_training_value  = options.get('is_training', None)
  dim_input          = options.get('dim_input', 1)
  dim_output         = options.get('dim_output',1)
  init_scopes        = options.get('init_scopes',['model']) #All weights
  constant_scopes    = options.get('constant_scopes', [])
  hyper_scopes       = options.get('hyper_scopes', [])
  alpha              = options.get('alpha', 0.001)
  beta               = options.get('beta', 0.001)
  learn_lr           = options.get('learn_lr', False)
  prefix             = options.get('prefix', '')
  activation         = options.get('activation', tf.nn.relu)
  scale              = options.get('scale', 0.01)
  allow_growth       = options.get('allow_growth', True)
  #Intervals and steps
  metatrain_steps    = options.get('metatrain_steps', 70000)
  eval_interval      = options.get('eval_interval', 5000)
  print_interval     = options.get('print_interval', 1000)
  save_interval      = options.get('save_interval', 1000)
  summary_interval   = options.get('summary_interval', 100)
  train_summary_dir  = options.get('train_smmary_dir', 'logs/farho_maml_train')
  eval_summary_dir   = options.get('eval_summary_dir', 'logs/farho_maml_eval')
  checkpoints_dir    = options.get('checkpoints_dir', 'logs/farho_maml_checkpoints')
  num_updates        = options.get('num_updates', 5)

  meta_batch_size    = options.get('meta_batch_size', 25)
  update_batch_size  = options.get('update_batch_size', 10)
  resume             = options.get('resume', False) #TODO not implemented yet

  def create_exp_string():
    exp_string = 'cls_'+str(dim_output)+'.mbs_'+str(meta_batch_size)+'.ubs_'+str(update_batch_size)+'.numstep'+str(num_updates)+'.alpha'+str(alpha)
    return exp_string

  exp_string = create_exp_string()

  if isinstance(dim_input, tuple) or isinstance(dim_input,list): #TODO assume flattened input
    dim_input = np.prod(dim_input)

  with tf.Graph().as_default():
    x = tf.placeholder(tf.float32, shape=(None,dim_input), name='x')
    y = tf.placeholder(tf.float32, shape=(None,dim_output), name='y')
    if is_training_value is not None:
      is_training = tf.placeholder(tf.bool, shape=(), name='is_training')
    else:
      is_training = None

    with tf.variable_scope('model'):
      logits, endpoints = maml_model.create_model_fn(x,
                                                     is_training=is_training,
                                                     prefix=prefix,
                                                     activation=activation,
                                                     scale=scale)

    with tf.variable_scope('init_weights'):
      variables = set()
      for scope in init_scopes:
        variables |= set(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=scope))
      print("Init Vars", variables)
      var_map = maml_model.create_learnable_initializers(variables, replace_from='model/')

    constant_vars = []
    for scope in constant_scopes:
      constant_vars.append(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=scope))
    print("Constant Vars", constant_vars)
    maml_model.make_variables_constant(constant_vars)

    hyper_vars = []
    for scope in hyper_scopes:
      hyper_vars.append(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=scope))
    maml_model.make_variables_hyperparameter(hyper_vars)

    norms   = [tf.norm(t) for t in var_map.values()]
    print('Learning model weight initializers (hyperparameters)')
    [print(e) for e in far.utils.hyperparameters()];

    with tf.variable_scope('losses'):
      loss, accuracy = maml_model.loss_fn(logits, y, is_regression=is_regression)

    with tf.variable_scope('optimization'):
      if learn_lr:
        lr = far.get_hyperparameter('lr', alpha)
      else:
        lr = alpha
    io_optim = far.GradientDescentOptimizer(lr)
    oo_optim = tf.train.AdamOptimizer(beta) #tf.train.AdamOptimizer(beta)

    farho = far.HyperOptimizer()
    run   = farho.minimize(loss, oo_optim, loss, io_optim, init_dynamics_dict=var_map)

    for i, v in enumerate(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)):
      print('[{}] -- name: {}, shape: {}'.format(i, v.op.name, v.shape))

    summary_op = tf.summary.merge_all()
    config_proto = tf.ConfigProto()
    config_proto.gpu_options.allow_growth = allow_growth
    with tf.Session(config=config_proto) as sess:
      sess.run(tf.global_variables_initializer())
      step = 1
      for dir_name in [train_summary_dir, eval_summary_dir, checkpoints_dir]:
        if not os.path.exists(dir_name):
          os.makedirs(os.path.join(os.getcwd(), dir_name))
      train_summary_writer = tf.summary.FileWriter(os.path.join(train_summary_dir, exp_string), graph=tf.get_default_graph())
      eval_summary_writer  = tf.summary.FileWriter(os.path.join(eval_summary_dir, exp_string), graph=tf.get_default_graph())
      saver = tf.train.Saver(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES), max_to_keep=10) #TODO: check this

      while step <= metatrain_steps:
        if 'generate' in dir(input_generator):
          inputs = input_generator.generate()
          tr_inputs, tr_labels = inputs[0].reshape((-1,1)), inputs[1].reshape((-1,1))
          val_inputs, val_labels = inputs[2].reshape((-1,1)), inputs[3].reshape((-1,1))
          inner_feed_dict={x: tr_inputs, y: tr_labels}
          outer_feed_dict={x: val_inputs, y: val_labels}
          if is_training_value is not None:
            inner_feed_dict.update({is_training: is_training_value})
            outer_feed_dict.update({is_training: False})
          run(num_updates, inner_objective_feed_dicts=inner_feed_dict,
              outer_objective_feed_dicts=outer_feed_dict, session=sess)
        else:
          raise NotImplementedError

        runMe = [loss]
        key2idx = {'loss': 0}
        if step % summary_interval == 0:
          runMe.extend([summary_op])
          key2idx.update({'summary': len(key2idx)})
        if accuracy is not None:
          runMe.extend([accuracy])
          key2idx.update({'accuracy': len(key2idx)})

        if step % print_interval == 0:
          runMe.extend(norms)
          for i in range(len(norms)):
            key2idx.update({'norms{}'.format(i):len(key2idx)})

        # TODO: Seems train, eval loss and acc are not computed correctly.
        result_list = sess.run(runMe, feed_dict=inner_feed_dict)

        if key2idx.has_key('summary'):
          train_summary_writer.add_summary(result_list[key2idx['summary']], step)
        if step % print_interval == 0:
          train_acc = result_list[key2idx['accuracy']] if key2idx.has_key('accuracy') else 'None'
          print('step: {}, loss: {}, train accuracy: {}'.format(step, result_list[key2idx['loss']], train_acc))
          np_norms = [result_list[key2idx['norms{}'.format(j)]] for j in range(len(norms))]
          print('norms: {}'.format(np_norms))

        if step % eval_interval == 0:
          print('-'*50)
          runMe = [loss, summary_op]
          key2idx = {'loss':0, 'summary':1}
          if accuracy is not None:
            runMe.extend([accuracy])
            key2idx['accuracy'] = len(key2idx)
          runMe.extend(norms)
          for i in range(len(norms)):
            key2idx['norms{}'.format(i)] = len(key2idx)

          result_list = sess.run(runMe, feed_dict=outer_feed_dict)

          eval_summary_writer.add_summary(result_list[key2idx['summary']], step)
          eval_acc = result_list[key2idx['accuracy']] if key2idx.has_key('accuracy') else 'None'
          print('step: {}, loss: {}, validation accuracy: {}'.format(step, result_list[key2idx['loss']], eval_acc))
          np_norms = [result_list[key2idx['norms{}'.format(j)]] for j in range(len(norms))]
          print('norms: {}'.format(np_norms))
          print('-'*50)

        if step % save_interval == 0:
          print('Saving checkpoint... ', end='')
          saver.save(sess, os.path.join(checkpoints_dir, exp_string + '_model.ckpt'), global_step=step)
          print('Done')
        step += 1

def farho_test(maml_model, input_generator, options):
  experiment         = options.get('experiment', 'sinusoid')
  add_regularization = options.get('add_regularization', False)
  is_regression      = options.get('is_regression', True)
  is_training_value  = options.get('is_training', None)
  dim_input          = options.get('dim_input', 1)
  dim_output         = options.get('dim_output',1)
  init_scopes        = options.get('init_scopes',['model']) #All weights
  alpha              = options.get('alpha', 0.001)
  beta               = options.get('beta', 0.001)
  learn_lr           = options.get('learn_lr', False)
  prefix             = options.get('prefix', '')
  activation         = options.get('activation', tf.nn.relu)
  scale              = options.get('scale', 0.01)
  allow_growth       = options.get('allow_growth', True)
  #Intervals and steps
  metatrain_steps    = options.get('metatrain_steps', 70000)
  eval_interval      = options.get('eval_interval', 5000)
  print_interval     = options.get('print_interval', 1000)
  save_interval      = options.get('save_interval', 1000)
  summary_interval   = options.get('summary_interval', 100)
  test_summary_dir   = options.get('test_summary_dir', 'logs/farho_maml_test')
  checkpoints_dir    = options.get('checkpoints_dir', 'logs/farho_maml_checkpoints')
  num_updates        = options.get('num_updates', 5)
  num_tests          = options.get('num_tests', 1000)

  meta_batch_size    = options.get('meta_batch_size', 25)
  update_batch_size  = options.get('update_batch_size', 10)
  resume             = options.get('resume', False) #TODO not implemented yet

  def create_exp_string():
    exp_string = 'cls_'+str(dim_output)+'.mbs_'+str(meta_batch_size)+'.ubs_'+str(update_batch_size)+'.numstep'+str(num_updates)+'.alpha'+str(alpha)
    return exp_string

  exp_string = create_exp_string()

  if isinstance(dim_input, tuple) or isinstance(dim_input,list): #TODO assume flattened input
    dim_input = np.prod(dim_input)

  with tf.Graph().as_default():
    x = tf.placeholder(tf.float32, shape=(None,dim_input), name='x')
    y = tf.placeholder(tf.float32, shape=(None,dim_output), name='y')
    if is_training_value is not None:
      is_training = tf.placeholder(tf.bool, shape=(), name='is_training')
    else:
      is_training = None

    with tf.variable_scope('model'):
      logits, endpoints = maml_model.create_model_fn(x,
                                                     is_training=is_training,
                                                     prefix=prefix,
                                                     activation=activation,
                                                     scale=scale)

    #with tf.variable_scope('init_weights'):
    variables = set()
    for scope in init_scopes:
      variables |= set(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=scope))
    #  print("Init Vars", variables)
    #  var_map = maml_model.create_learnable_initializers(variables, replace_from='model/')
    available_var_map = {}
    for v in variables:
      name = v.op.name.replace('model/', 'init_weights/')
      available_var_map[name] = v
    init_saver = tf.train.Saver(available_var_map)
    model_file = tf.train.latest_checkpoint(checkpoints_dir)
    def initializer_fn(sess):
      init_saver.restore(sess, model_file)

    with tf.variable_scope('losses'):
      loss, accuracy = maml_model.loss_fn(logits, y, is_regression=is_regression)

    with tf.variable_scope('optimization'):
      #if learn_lr:
      #  lr = far.get_hyperparameter('lr', alpha)
      #else:
      #  lr = alpha
      #io_optim = far.GradientDescentOptimizer(lr)
      optimizer = tf.train.GradientDescentOptimizer(alpha) #tf.train.AdamOptimizer(beta)
      train_op  = optimizer.minimize(loss)

    for i, v in enumerate(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)):
      print('[{}] -- name: {}, shape: {}'.format(i, v.op.name, v.shape))

    summary_op = tf.summary.merge_all()
    config_proto = tf.ConfigProto()
    config_proto.gpu_options.allow_growth = allow_growth

    pre_losses = []
    update_losses = [[] for _ in range(num_updates)]
    post_losses = []
    for test_id in range(num_tests):
      print('Test #{}/{}'.format(test_id+1,num_tests))
      if 'generate' in dir(input_generator):
        inputs = input_generator.generate()
        tr_inputs, tr_labels = inputs[0].reshape((-1,1)), inputs[1].reshape((-1,1))
        val_inputs, val_labels = inputs[2].reshape((-1,1)), inputs[3].reshape((-1,1))
        inner_feed_dict={x: tr_inputs, y: tr_labels}
        outer_feed_dict={x: val_inputs, y: val_labels}
        if is_training_value is not None:
          inner_feed_dict.update({is_training: is_training_value})
          outer_feed_dict.update({is_training: False})
      else:
        raise NotImplementedError
      with tf.Session(config=config_proto) as sess:
        sess.run(tf.global_variables_initializer())
        initializer_fn(sess)
        step = 0
        pre_loss = sess.run(loss, feed_dict=inner_feed_dict)
        pre_losses.append(pre_loss)
        while step < num_updates:
          sess.run(train_op, feed_dict=inner_feed_dict)
          update_loss = sess.run(loss, feed_dict=inner_feed_dict)
          update_losses[step].append(update_loss)
          step += 1
        post_loss = sess.run(loss, feed_dict=outer_feed_dict)
        post_losses.append(post_loss)

    print('pre_loss: {}'.format(np.mean(np.asarray(pre_losses))))
    for i,update_loss in enumerate(update_losses):
      print('update_loss{}: {}'.format(i, np.mean(np.asarray(update_loss))))
    print('post_loss: {}'.format(np.mean(np.asarray(post_losses))))
